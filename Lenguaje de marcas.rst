Lenguajes de marcas
====================
**Autor:** *Daniel Jesus Umpiérrez Del Río*

Reseña histórica
----------------
La idea de los lenguajes de marcado está basada en la  forma en que antiguamente se daba formato a los documentos en las imprentas.

Este formato se establecía mediante el uso de una reseña en tinta de color azul que daba las especificaciones a seguir en todo el documento relacionados con la manera en que este debía ser formateado.

Esta idea se traslado a la informática en la forma de lo que hoy conocemos como lenguaje de marcas. También se le conoce como lenguaje de marcado o lenguajes de etiquetas, pudiendo usarse cualquiera de esos nombres indistintamente para referirnos a lo mismo.

Esta idea surge como concepto en 1967 por parte de *William W. Tunnicliffe* aunque no fue hasta los años 80 cuando comenzó a generalizarse. Durante esta década destaca la aparición del `GML <http://es.wikipedia.org/wiki/Generalized_Markup_Language>`_ (*Generalized Markup Language*) que consiguió un gran éxito y una rápida expansión por lo que  consigue su estandarización en el año 1986 por la `ISO <http://es.wikipedia.org/wiki/Organización_Internacional_de_Normalización>`_ (*Internacional Standard Organization*) tomando el nombre de `SGML <http://es.wikipedia.org/wiki/SGML>`_.

Definición
----------
Aunque es difícil definir a grandes rasgos y en pocas palabras lo que es un lenguaje de marcas, podríamos decir que es un conjunto de instrucciones intercaladas dentro de un texto,  que nos proporcionan unas directrices sobre cómo debe ser tratado, estructurado o representado dicho texto. Estas instrucciones, también llamadas etiquetas o marcas, deben estar separadas de la información de una manera clara y sencilla.

Clases de lenguaje de marcas
----------------------------
Los lenguajes de marcas se clasifican de la siguiente manera:

1. **Presentación**: tratan de sobre el formato que se le va a dar al texto del fichero en cuestión y sobre como será la presentación del texto de cara al usuario final. Es útil si nuestro proyecto es pequeño ya que es sencillo de utilizar pero está totalmente desaconsejado en proyectos grandes ya que se hace difícil su mantenimiento. Por ejemplo si deseamos cambiar el color letra de los encabezados deberíamos ir buscándolos por todo el texto y cambiándolos uno a uno, lo cual es muy tedioso. Un ejemplo sobre este tipo de lenguajes es el `Rich Text Format <http://es.wikipedia.org/wiki/Rich_Text_Format>`_ usado por `Microsoft <http://es.wikipedia.org/wiki/Microsoft>`_ en su aplicación `Wordpad <http://es.wikipedia.org/wiki/WordPad>`_.
2. **Procedimientos**: este tipo de lenguaje de marcas también está orientado a indicar como será el formato que tendrá finalmente el contenido del fichero. La diferencia está en que las etiquetas se deben interpretar en el riguroso orden de aparición, teniendo que quedar perfectamente definido cuando acaba la etiqueta en cuestión. Ademas otra diferencia es que usuario mientras edita el texto esta viendo las etiquetas que contiene, cosa que no ocurre con los lenguajes de presentación que solo se ve el contenido. Un ejemplo es el lenguaje `HTML <http://es.wikipedia.org/wiki/HTML>`_, usado para elaborar páginas web. En este lenguaje, si queremos indicar que un texto debe aparecer remarcado como negrita, debemos poner una etiqueta ``<bold>`` de apertura y otra ``</bold>`` de cierre. Estas dos etiquetas indican que todo el texto que se encuentre entre ambas aparecerá resaltado en **negro**.
3. **Descriptivo**: a diferencia de los anteriores, este tipo de lenguajes de marcas no trata de establecer la manera en que el texto es presentando, sino más bien de describir o contextualizar los datos que se encuentran dentro de las etiquetas. Un ejemplo de este tipo de lenguaje de marcas es el famoso **XML**.

Características de los lenguajes de marcas
------------------------------------------
* **Portabilidad**: el tipo de archivo usado para escribir en lenguajes de marcas es el texto plano, por lo que no necesita de software intermedio que decodifique su contenido para ser leído como si ocurre con los ficheros binarios. Esto quiere decir que con un editor de texto plano por simple que sea, como por ejemplo el "`Bloc de notas <http://es.wikipedia.org/wiki/Bloc_de_notas>`_" de `Microsoft <http://es.wikipedia.org/wiki/Microsoft>`_, podemos ponernos manos a la obra en el uso de este tipo de lenguajes. El trabajar con ficheros de texto plano nos da una independencia en cuanto al tipo de plataforma en la que desarrollamos nuestros proyectos, ya que cualquier arquitectura o sistema operativo es capaz de leer este tipo de ficheros. Esto nos permite ampliar las fronteras del intercambio de información.
* **Compacidad**: con compacidad nos referimos a que las instrucciones del lenguaje de marcado se encuentran integradas con el contenido del fichero de una manera "compacta". Todo se encuentra en un único fichero.
* **Fácil procesado**:  una vez creado un proyecto en un lenguaje de marcas, es muy fácil y sencillo darle diferentes usos y adaptarlo según las necesidades. Se puede procesar por ejemplo, para crear documentación, páginas web, impresión de libros y documentos en papel, etc.
* **Flexibilidad**: son muy versátiles, se pueden emplear casi para cualquier cosa. Al ser metalenguajes se pueden crear nuevos lenguajes a partir de otro, con el fin de resolver un problema particular, haciéndose todo esto de manera sencilla.

Herramientas para trabajar con lenguajes de marcas 
--------------------------------------------------
Aunque en principio, con un simple editor de texto tendríamos la posibilidad de desarrollar contenido en cualquier lenguaje de marcas, es evidente que si no nos apoyamos en un software especifico para desarrollo el trabajo se nos puede hacer largo y pesado.

Existen muchos tipos de herramientas distintos tanto libres como comerciales que nos pueden echar una mano a la hora de crear nuestro código.   

A continuación procedo a citar solo algunas de las muchísimas herramientas para desarrollo en lenguajes de marcas:

+--------------------+---------------------------------------------------------------------------------------+
|Herramienta         |   Descripción                                                                         |
+====================+=======================================================================================+
| Notepad++          | Version libre, mejorada y muy versátil de la aplicación Notepad de Windows            |
+--------------------+---------------------------------------------------------------------------------------+
| Eclipse            | Herramienta libre, robusta y flexible que editar lenguajes de todo tipo incluido XML  |
+--------------------+---------------------------------------------------------------------------------------+
| Dreamweaver        | Software comercial de gran éxito para desarrollo de páginas web de la compañía Adobe  |
+--------------------+---------------------------------------------------------------------------------------+
| Frontpage          | Software incluido en la suite Office de Microsoft para elaboración de páginas         |
+--------------------+---------------------------------------------------------------------------------------+


Ejemplos de lenguajes de marcas
-------------------------------

Esto es solo una pequeña muestra de la gran cantidad de lenguajes de marcas que existen:

+--------------------+---------------------------------------------------------------------------------------+
|Lenguaje            |   Descripción                                                                         |
+====================+=======================================================================================+
| GML                | Primer lenguaje de marcas ideado por IBM                                              |
+--------------------+---------------------------------------------------------------------------------------+
| SGML               | Lenguaje basado en GML y estandarizado por ISO en 1986                                |
+--------------------+---------------------------------------------------------------------------------------+
| XML                | Extensible Markup Language, deriva de SGML y fue creado por la W3C                    |
+--------------------+---------------------------------------------------------------------------------------+
| HTML               | El HyperText Markup Language nace con el propósito de la elaboración de páginas web   |
+--------------------+---------------------------------------------------------------------------------------+
| Tex                | Usado por la comunidad científica para escritura de complejas fórmulas matemáticas    |
+--------------------+---------------------------------------------------------------------------------------+
| RTF                | Rich Text Format creado por Microsoft usado por el famoso Wordpad de Windows          |
+--------------------+---------------------------------------------------------------------------------------+
| reStructured text  | Usado para escribir esta Wiki, se trata de un lenguaje ligero y de fácil uso          |
+--------------------+---------------------------------------------------------------------------------------+
| RSS                | Really Simple Syndication usado para compartir información en la red                  |
+--------------------+---------------------------------------------------------------------------------------+
| SVG                | Scalable Vector Graphics basado en XML, su uso se basa en la creación de gráficos     |
+--------------------+---------------------------------------------------------------------------------------+
| MathML             | Basado también en XML se usa para expresar notación de formulas matemáticas           |
+--------------------+---------------------------------------------------------------------------------------+

A continuación una imagen sobre los lenguajes derivados de SGML a través del tiempo (hasta 2007):

.. image:: http://mobilewireless.files.wordpress.com/2009/02/mobile_web_standards_evolution_vector2.jpg

Conclusión
----------
Los lenguajes de marcas surgen para tratar de hacer mas fácil el intercambio de información entre sistemas y como una manera de integrar en un solo fichero la estructura y el formato.
Por otro lado son unos lenguajes muy flexibles y adaptables de manera sencilla sea cual sea nuestra meta.

Webgrafía
------------
- http://es.wikipedia.org/wiki/Lenguaje_de_marcado
- http://en.wikipedia.org/wiki/Markup_language
- http://es.wikipedia.org/wiki/Extensible_Markup_Language
- http://es.wikipedia.org/wiki/HTML
- http://es.wikipedia.org/wiki/ReStructuredText
- http://es.wikipedia.org/wiki/Scalable_Vector_Graphics
- http://es.wikipedia.org/wiki/MathML
- http://es.wikipedia.org/wiki/Rich_Text_Format
- http://www.alegsa.com.ar/Dic/lenguaje%20de%20marcas.php
- http://elies.rediris.es/elies24/fernandezvalmayor_cap3.htm
- http://centrodeartigos.com/articulos-para-saber-mas/article_45302.html
- http://www.hipertexto.info/documentos/lenguajes_h.htm
- http://www.monografias.com/trabajos94/html-lenguajes-marcas/html-lenguajes-marcas2.shtml
- http://mobilewireless.wordpress.com/